// Getting current time in Epoch
#include <NTPClient.h>
#include <WiFiUdp.h>

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", 0);

// Server Controller Details
#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>

// Soil Humidity Controller
# define DIGI0 D0
# define ANA A0

double analogValue = 0.0;
double tempValue = 0.0;
int digitalValue = 0;
double analogVolts = 0.0;
unsigned long timeHolder = 0;

// Motor Controller
# define DIGI1 D1

// Temperature Controller
#include "DHT.h"

# define DHTTYPE DHT22
uint8_t DHTPin = D5;
DHT dht(DHTPin, DHTTYPE);

float Temperature;
float Humidity;

void setup() {
  // Setup
  Serial.begin(115200);

  // WiFi Setup
  WiFi.begin("<ssid>", "<password>");
  while (WiFi.status() != WL_CONNECTED) {  //Wait for the WiFI connection completion
    delay(500);
  }

  // Current Time
  timeClient.begin();

  // Soil Humidity Setup
  pinMode(ANA, INPUT);
  pinMode(DIGI0, INPUT);

  // Motor Setup
  pinMode(DIGI1, OUTPUT);

  // Temperature Setup
  pinMode(DHTPin, INPUT);
  dht.begin();
}

void loop() {
  // Update current Time
  timeClient.update();
  long epochTime = timeClient.getEpochTime();

  //  Soil Humidity get data
  analogValue = analogRead(ANA);
  digitalValue = digitalRead(DIGI0);

  // Temperature get data
  Temperature = dht.readTemperature(); // Gets the values of the temperature
  Humidity = dht.readHumidity(); // Gets the values of the humidity

  postData("temperature", temperatureDataPayload( Temperature, Humidity, epochTime ));
  postData("soil-humidity", soilHumidDataPayload( analogValue, digitalValue, epochTime ));

  // Motor switch - depends on the digital/
  if (analogValue > 1000) {
    // Switch on
    digitalWrite(DIGI1, HIGH);
    postData("pump", pumpDataPayload( 1, epochTime ));
  } else {
    // Switch off
    digitalWrite(DIGI1, LOW);
  }

  delay(1000);
}

void postData( String destination, String payload ) {
  // Post data to server
  if ( WiFi.status() == WL_CONNECTED ) {
    HTTPClient http;    //Declare object of class HTTPClient

    String url = "http://<host>/api/" + destination;

    http.begin(url);
    http.addHeader("Content-Type", "application/json");

    int httpCode = http.POST(payload);

    http.end();
  }

}

// Create Temperature endpoint payload
String temperatureDataPayload( float temp, float humid, long times ) {
  String message = "{ \"device\":\"nodemcu_plant\", \"measurement\": [ { \"celcius\":" + String(temp, 2) + ", \"humidity\": " + String(humid, 2) + ", \"time\": " + String(times) + " }, { \"user\": \"potplant\" } ] }";
  return message;
}

// Create Soil-Humidity endpoint payload
String soilHumidDataPayload( double analogValue, int digitalValue, long times ) {
  String message = "{ \"device\":\"nodemcu_plant\", \"measurement\": [ { \"analogue\":" + String(analogValue) + ", \"digital\": " + String(digitalValue) + ", \"time\": " + String(times) + " }, { \"user\": \"potplant\" } ] }";
  return message;
}

// Create Pump endpoint payload
String pumpDataPayload( int pump, long times ) {
  String message = "{ \"device\":\"nodemcu_plant\", \"measurement\": [ { \"pump\":" + String(pump) + ", \"time\": " + String(times) + " }, { \"user\": \"potplant\" } ] }";
  return message;
}

float roundFloatValue( float i ){
  return (float)((int)(i * 100 + 0.5))/100;
}
